def dummy_decorator(func):
    def inner(*args):
        print 'All args to decorated function', args
        return func(*args)
    return inner

@dummy_decorator
def to_second_power(arg):
    return arg * 2

@dummy_decorator
def add_two(x, y):
    return x + y

@dummy_decorator
def my_sum(*args):
    return sum(args)


print to_second_power(2)
print add_two(2,3)
print my_sum(1,2,3,4)


class MyClass(object):
    foo = 'bar'
    def __getattribute__(self, name):
        #print 'in getattribute'
        if name is 'name':
            return 'I am the walrus'
        return super(MyClass, self).__getattribute__(name)

    def __getattr__(self, name):
        #print 'in __getattr__'
        raise AttributeError


print MyClass().foo
print MyClass().name
print MyClass().missing
