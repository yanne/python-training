import json
import xml.etree.ElementTree as ET
import random

import tornado.ioloop
import tornado.web
import tornado.template


class ContactEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Contact):
            return {'name': obj.name, 'email': obj.email}
        return super(ContactEncoder, self).default(obj)


class Contact(object):

    def __init__(self, name, email):
        self.name = name
        self.email = email


class MainHandler(tornado.web.RequestHandler):

    def get(self):
        if 'as_json' in self.request.arguments:
            self.write(self.render_as_json(self.request.arguments['as_json']))
        elif 'as_xml' in self.request.arguments:
            self.set_header('content-type', 'application/xml')
            self.write(self.render_as_xml())
        else:
            self.write(self.render_main())

    def render_as_json(self, request_index):
        import time
        randomizer = random.Random()
        sleeptime = 1-int(request_index[0])/10.0
        print 'Sleeping', sleeptime
        time.sleep(sleeptime)
        return json.dumps({
            'contacts': self.get_contacts(),
            'index': request_index[0]
        }, cls=ContactEncoder)

    def render_as_xml(self):
        root = ET.Element('contacts')
        for contact in self.get_contacts():
            contact_node = ET.SubElement(root, 'contact')
            name_node = ET.SubElement(contact_node, 'name')
            name_node.text = contact.name
            email_node = ET.SubElement(contact_node, 'email')
            email_node.text = contact.email
        return ET.tostring(root)

    def render_main(self):
        return tornado.template.Template("""
<html>
  <h2>My Contacts</h2>
  <table>
  <tr><th>Name</th><th>Email</th><tr>
  {% for contact in contacts %}
    <tr><td>{{ contact.name }}</td><td>{{ contact.email }}</td></tr>
  {% end %}
</html>
""").generate(contacts=self.get_contacts())

    def get_contacts(self):
        return [Contact('FOO', 'foo@bar')]


if __name__ == '__main__':
    application = tornado.web.Application([
        (r"/", MainHandler),
    ], debug=True)
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
