import tornado.ioloop
from tornado.httpclient import AsyncHTTPClient
from tornado import gen

@gen.coroutine
def fetch_request(index):
    http_client = AsyncHTTPClient()
    print 'starting request %d' % index
    response = yield http_client.fetch('http://localhost:8888/?as_json=%s' % index)
    print response.body


if __name__ == '__main__':
    for i in xrange(5):
        fetch_request(i)

    tornado.ioloop.IOLoop.instance().start()
