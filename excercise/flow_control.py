import unittest


def fizzbuzz(num):
    """Returns next value in fizzbuzz sequence.

    Fizzbuzz sequence is a sequence of numbers where:
      if the number is divisible by three, it is replaced with word fizz
      if the number is divisible by five, it is replaced with word buzz
      if the number is divisible by three and five, it is replaced with word fizzbuzz
      otherwise the number is unchanged
    """
    raise NotImplementedError('Implement this')



def my_get(a_dict, key):
    """Returns value from a_dict with key.

    If value is not present, raises RuntimeError.
    """
    raise NotImplementedError('Implement this')



def my_get_with_default(a_dict, key, default=None):
    """Returns value from a_dict with key.

    If value is not present, returns None.
    Uses `my_get` internally.
    """
    raise NotImplementedError('Implement this')


class TestFlowControl(unittest.TestCase):

    def test_fizzbuzz(self):
        self.assertEquals(fizzbuzz(1), '1')
        self.assertEquals(fizzbuzz(3), 'fizz')
        self.assertEquals(fizzbuzz(5), 'buzz')
        self.assertEquals(fizzbuzz(11), '11')
        self.assertEquals(fizzbuzz(15), 'fizzbuzz')

    def test_my_get(self):
        a_dict = {'key': 'value'}
        self.assertEquals(my_get(a_dict, 'key'), 'value')
        self.assertRaises(RuntimeError, my_get, a_dict, 'does not exist')

    def test_my_get_with_default(self):
        a_dict = {'key': 'value'}
        self.assertEquals(my_get_with_default(a_dict, 'key'), 'value')
        self.assertEquals(my_get_with_default(a_dict, 'does not exist'), None)
        self.assertEquals(my_get_with_default(a_dict, 'does not exist', default=1), 1)


if __name__ == "__main__":
    unittest.main()
