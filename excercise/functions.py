import unittest


def returns_argument(arg):
    """Returns arg."""
    raise NotImplementedError('Implement this')


def add_two(fst, snd):
    """Returns sum of fst and snd."""
    raise NotImplementedError('Implement this')


def multiply_by(value, multiplier=1):
    """Returns product of value and multiplier."""
    raise NotImplementedError('Implement this')


def catenate(separator, *texts):
    """Catenates list of strings, *texts using separator."""
    raise NotImplementedError('Implement this')


def search_needle(**kwargs):
    raise NotImplementedError('Implement this')


class TestFunctions(unittest.TestCase):

    def test_return_value(self):
        self.assertEquals(returns_argument('foo'), 'foo')
        self.assertEquals(returns_argument([1, 2, 3]), [1, 2, 3])

    def test_add_two(self):
        self.assertEquals(add_two(1, 3), 4)
        self.assertEquals(add_two(-1, 31), 30)

    def test_multiply_by(self):
        result = multiply_by(3)
        self.assertEquals(result, 3)
        result = multiply_by(4, 5)
        self.assertEquals(result, 20)
        result = multiply_by(4, multiplier=3)
        self.assertEquals(result, 12)

    def test_catenate(self):
        result = catenate(',', 'foo', 'bar')
        self.assertEquals(result, 'foo,bar')
        result = catenate(' ', *['a', 'complete', 'sentence'])
        self.assertEquals(result, 'a complete sentence')

    def test_search_needle(self):
        self.assertEquals(search_needle(needle='sharp'), 'sharp')
        self.assertEquals(search_needle(not_a_needle='blunt'), 'needle not found')


if __name__ == '__main__':
    unittest.main()
