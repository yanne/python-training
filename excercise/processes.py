import subprocess
import unittest
import time
import tempfile


def read_file(path):
    with open(path) as input:
        return input.read()

def list_directory(path):
    return subprocess.check_output(['ls', '-l', path])


def non_terminating_process():
    return subprocess.Popen(['tail', '-f', 'hello.py'], stdout=tempfile.TemporaryFile())


class TestFilesAndProcesses(unittest.TestCase):

    def test_reading_file(self):
        self.assertEquals(read_file('hello.py'), "print 'Hello, world!'\n")

    def test_reading_process(self):
        output = list_directory('.')
        self.assertTrue(len(output) > 1 )

    def test_terminating_process(self):
        process = non_terminating_process()
        while process.poll() is None:
            print 'waiting for death'
            time.sleep(0.1)
            process.kill()


if __name__ == "__main__":
    unittest.main()
