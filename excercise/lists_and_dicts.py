import unittest


def contains(a_list, element):
    """Returns True if a_list contains element, otherwise False."""
    raise NotImplementedError('Implement this')



def my_map(a_list, f):
    """Applies f to all elements of a_list, returns resulting list"""
    raise NotImplementedError('Implement this')



def dict_contains(a_dict, value):
    """Returns True if values of a_dict contains value, otherwise False."""
    raise NotImplementedError('Implement this')


class TestCollections(unittest.TestCase):

    def test_list_contains(self):
        numbers = [1,2,3]
        self.assertTrue(contains(numbers, 1))
        self.assertFalse(contains(numbers, 5))

    def test_map(self):
        numbers = [1, 2, 3]
        def plus_two(number):
            return number + 2
        self.assertEquals(my_map(numbers, plus_two), [3, 4, 5])

    def test_dict_contains(self):
        a_dict = {'key': 'value', 'key2': 'second value'}
        self.assertTrue(dict_contains(a_dict, 'value'))
        self.assertFalse(dict_contains(a_dict, 'third value'))


if __name__ == "__main__":
    unittest.main()
