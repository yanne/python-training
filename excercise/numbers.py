import unittest


def add(*args):
    """Returns sum of all arguments."""
    raise NotImplementedError('Implement this')


def multiply(fst, snd):
    """Returns product of fst and snd."""
    raise NotImplementedError('Implement this')


def divide(fst, snd):
    """Returns quotient of fst and snd.

    Returns 0 if divisor is 0.
    """
    raise NotImplementedError('Implement this')


class TestArithmetic(unittest.TestCase):

    def test_add_two_numbers(self):
        self.assertEquals(add(1,2), 3)
        self.assertEquals(add(22,13), 35)

    def test_multiply(self):
        self.assertEquals(multiply(1,2), 2)
        self.assertEquals(multiply(10,2), 20)

    def test_safe_division(self):
        self.assertEquals(divide(4,2), 2)
        self.assertEquals(divide(7,3), 2)
        self.assertEquals(divide(7,0), 0)
        self.assertEquals(divide(0,0), 0)
        self.assertEquals(divide(0,3), 0)

    def test_add_many(self):
        # Modify add to add together any number of arguments
        self.assertEquals(add(1,2,3), 6)
        self.assertEquals(add(1,2,3,100,1000), 1106)

    def test_multiply_should_handle_strings(self):
        # Modify multiply to work also if arguments are given as strings
        self.assertEquals(multiply("2", "5"), 10)
        self.assertEquals(multiply(4, "5"), 20)


if __name__ == "__main__":
    unittest.main()
