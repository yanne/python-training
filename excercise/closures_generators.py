import unittest


def make_adder(value):
    """Returns a function that takes one argument and adds value to it."""
    raise NotImplementedError('Implement this')


def fibs():
    """Returns an infinite generator of fibonacci sequence.

    Calling next() will always return the next fibonacci number.
    """
    raise NotImplementedError('Implement this')


class TestClosuresAndGenerators(unittest.TestCase):

    def test_make_adder(self):
        add2 = make_adder(2)
        self.assertEquals(add2(3), 5)
        add10 = make_adder(10)
        self.assertEquals(add10(3), 13)

    def test_fibonacci_generator(self):
        fibos = fibs()
        self.assertEquals(fibos.next(), 1)
        self.assertEquals(fibos.next(), 1)
        prev, prev2 = 1, 1
        for i in xrange(100):
          f = fibos.next()
          self.assertEquals(f, prev+prev2)
          prev, prev2 = f, prev


if __name__ == "__main__":
    unittest.main()
