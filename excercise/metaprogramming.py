import unittest

def to_power_of_two(func):
    raise NotImplementedError

@to_power_of_two
def multiply(x, y):
    return x * y


class MagicClass(object):
    """Has all attributes, attribute value equals the length of attribute name."""


class TestMetaprogramming(unittest.TestCase):

    def test_decorators(self):
        self.assertEquals(multiply(3, 3), 81)

    def test_magic_class(self):
        magic = MagicClass()
        self.assertEquals(magic.foo, 3)
        self.assertEquals(magic.magic, 5)
        self.assertEquals(magic.woah_its_magic, 14)


if __name__ == "__main__":
    unittest.main()
