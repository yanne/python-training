import unittest


class MyList(object):

    def __init__(self, *elems):
        self._elems = elems

    def __len__(self):
        raise NotImplementedError

    def __eq__(self, other):
        raise NotImplementedError

    def __unicode__(self):
        raise NotImplementedError

    def __getitem__(self, i):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError


class TestMyList(unittest.TestCase):

    def test_lenght(self):
        l = MyList()
        self.assertEquals(len(l), 0)
        l = MyList(1, 2)
        self.assertEquals(len(l), 2)

    def test_equality(self):
        l1 = MyList(1)
        l2 = MyList(1)
        self.assertEquals(l1, l2)

    def test_truthiness(self):
        self.assertFalse(MyList())
        self.assertTrue(MyList(1,2))

    def test_unicode(self):
        self.assertEquals(unicode(MyList(1,2)), '[1, 2]')

    def test_indexing(self):
        l = MyList(4,5,6)[2]
        self.assertEquals(l, 6)

    def test_iteration(self):
        expected = 1
        for elem in MyList(1,2,3):
            self.assertEquals(elem, expected)
            expected += 1

    def test_lenght_property(self):
        self.assertEquals(MyList().length, 0)
        self.assertEquals(MyList(1,2,3).length, 3)




if __name__ == "__main__":
    unittest.main()
