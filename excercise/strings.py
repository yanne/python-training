import unittest


def length(text):
    """Returns the lenght of text."""
    raise NotImplementedError('Implement this')


def without(text, to_be_removed):
    """Returns text with all chars in to_be_removed removed"""
    raise NotImplementedError('Implement this')


def count_distinct(text):
    """Returns the number of distinct characters in text."""
    raise NotImplementedError('Implement this')


def tokens(text, separator='.'):
    """Returns a list containing tokens os text, separated by separator."""
    raise NotImplementedError('Implement this')


class TestStrings(unittest.TestCase):

    def test_get_string_length(self):
        self.assertEquals(length(''), 0)
        self.assertEquals(length('foo'), 3)

    def test_get_string_without(self):
        self.assertEquals(without('abba', 'a'), 'bb')
        self.assertEquals(without('abba', 'cd'), 'abba')
        self.assertEquals(without('foobar', 'ob'), 'far')

    def test_count_discint_chars(self):
        self.assertEquals(count_distinct('abba'), 2)
        self.assertEquals(count_distinct(u'abcdfdaf'), 5)

    def test_tokens(self):
        self.assertEquals(tokens('foo.bar'), ['foo', 'bar'])
        self.assertEquals(tokens('..foo..bar..'), ['foo', 'bar'])
        self.assertEquals(tokens('hello'), ['hello'])


if __name__ == '__main__':
    unittest.main()
