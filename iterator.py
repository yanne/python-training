class Iterator(object):
    def __init__(self, max):
        self._index = 0
        self._max = max

    def __iter__(self):
        return self

    def next(self):
        if self._index <= self._max:
            value = self._index
            self._index += 1
            return value
        else:
            raise StopIteration


class SequenceLike(object):
    def __init__(self):
        self.sequence = 'abcdefg'

    def __getitem__(self, index):
        return self.sequence[index]


class Generator(object):
    def __init__(self):
        self.value = 1

    def __iter__(self):
        while self.value <= 2048:
            yield self.value
            self.value *= 2


print 'Iterating over iterator'
for elem in Iterator(5):
    print elem


print 'Iterating over a seq'
for elem in SequenceLike():
    print elem


print 'Iterating over generator'
for elem in Generator():
    print elem
