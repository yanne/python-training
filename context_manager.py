import StringIO

class ContextManagerExample(object):
    def __enter__(self):
        self._buffer = StringIO.StringIO()
        return self._buffer

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            print 'OH NO, an exception', exc_type, exc_val
        print self._buffer.getvalue()


with ContextManagerExample() as output:
    output.write('No morjens!')

# with ContextManagerExample() as output:
#     output.write('No')
#     raise RuntimeError('Hanskat tiskiin')
